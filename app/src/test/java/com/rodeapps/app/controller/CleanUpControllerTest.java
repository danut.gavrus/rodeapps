package com.rodeapps.app.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CleanUpControllerTest {

    @Test
    void ping() {
        CleanUpController cleanUpController = new CleanUpController();
        String response = cleanUpController.ping();
        assertEquals("pong", response);
    }

}
