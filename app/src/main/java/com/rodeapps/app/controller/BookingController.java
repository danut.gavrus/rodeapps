package com.rodeapps.app.controller;

import com.google.gson.Gson;
import com.rodeapps.app.dto.BookingDTO;
import com.rodeapps.app.entity.Booking;
import com.rodeapps.app.service.BookingService;
import com.rodeapps.app.service.CityService;
import com.rodeapps.app.service.CleanerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/booking")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private CleanerService cleanerService;

    @Autowired
    private CityService cityService;

    @PostMapping("/add")
    public ResponseEntity<String> postAddBooking(@RequestBody BookingDTO bookingDTO) {
        Booking booking = Booking.asBooking(bookingDTO);
        booking.setCleaner(cleanerService.getById(bookingDTO.getCleanerId()));
        booking.setCity(cityService.getById(bookingDTO.getCityId()));
        bookingService.addBooking(booking);
        return ResponseEntity.ok("Booking added.");
    }

    @GetMapping("/all")
    public ResponseEntity<String> getBookings() {
        List<BookingDTO> bookings = new ArrayList<>();
        for (Booking booking : bookingService.getAll()) {
            bookings.add(BookingDTO.asBookingDTO(booking));
        }
        return ResponseEntity.ok(new Gson().toJson(bookings));
    }
}
