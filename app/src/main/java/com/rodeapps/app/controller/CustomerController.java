package com.rodeapps.app.controller;

import com.google.gson.Gson;
import com.rodeapps.app.dto.CityAndDateDTO;
import com.rodeapps.app.dto.CleanerDTO;
import com.rodeapps.app.entity.City;
import com.rodeapps.app.entity.Cleaner;
import com.rodeapps.app.service.CityService;
import com.rodeapps.app.service.CleanerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/customer")
public class CustomerController {

    @Autowired
    private CleanerService cleanerService;

    @Autowired
    private CityService cityService;

    @GetMapping("/cleaners")
    public ResponseEntity<String> getCleaners(@RequestBody CityAndDateDTO cityAndDateDTO) {
        City city = cityService.getById(cityAndDateDTO.getCityId());
        List<CleanerDTO> cleaners = new ArrayList<>();
        for (Cleaner cleaner : cleanerService.getCleanersByCity(city)) {
            cleaners.add(CleanerDTO.asCleanerDTO(cleaner));
        }
        return ResponseEntity.ok(new Gson().toJson(cleaners));
    }

}
