package com.rodeapps.app.controller;

import com.google.gson.Gson;
import com.rodeapps.app.dto.AuthDTO;
import com.rodeapps.app.dto.UserDTO;
import com.rodeapps.app.service.UserService;
import com.rodeapps.app.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping()
public class CleanUpController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("/ping")
    public String ping() {
        return "pong";
    }

    @GetMapping("/home")
    public String getHome() {
        return "Home";
    }

    @PostMapping("/authenticate")
    public ResponseEntity<String> postAuthenticationToken(@RequestBody UserDTO userDTO) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword()));
        } catch (BadCredentialsException e) {
            e.printStackTrace();
            return ResponseEntity.ok("Wrong username or password");
        }
        UserDetails userDetails = userService.loadUserByUsername(userDTO.getUsername());
        AuthDTO authDTO = new AuthDTO();
        authDTO.setJwt(jwtUtil.generateToken(userDetails));
        return ResponseEntity.ok(new Gson().toJson(authDTO));
    }

}
