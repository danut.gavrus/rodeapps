package com.rodeapps.app.controller;

import com.rodeapps.app.dto.CityDTO;
import com.rodeapps.app.entity.City;
import com.rodeapps.app.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(path = "/city")
public class CityController {

    private static final String CITY_ID = "id";

    @Autowired
    private CityService cityService;

    @PostMapping("/add")
    public ResponseEntity<String> postAddCity(@RequestBody CityDTO cityDTO) {
        cityService.addCity(City.asCity(cityDTO));
        return ResponseEntity.ok("City added.");
    }

    @PostMapping("/edit")
    public ResponseEntity<String> postEditCity(@RequestBody CityDTO cityDTO) {
        City city = City.asCityWithIdSet(cityDTO);
        cityService.editCity(city);
        return ResponseEntity.ok("City edited.");
    }

    @PostMapping("/delete")
    public ResponseEntity<String> postDeleteCity(@RequestBody Map<String, Long> jsonRequest) {
        cityService.deleteCity(jsonRequest.get(CITY_ID));
        return ResponseEntity.ok("City deleted.");
    }

}
