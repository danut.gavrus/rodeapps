package com.rodeapps.app.controller;

import com.rodeapps.app.dto.CleanerDTO;
import com.rodeapps.app.entity.City;
import com.rodeapps.app.entity.Cleaner;
import com.rodeapps.app.service.CityService;
import com.rodeapps.app.service.CleanerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(path = "/cleaner")
public class CleanerController {

    private static final String CLEANER_ID = "id";

    @Autowired
    private CleanerService cleanerService;

    @Autowired
    private CityService cityService;

    @PostMapping("/add")
    public ResponseEntity<String> postAddCleaner(@RequestBody CleanerDTO cleanerDTO) {
        Cleaner cleaner = Cleaner.asCleaner(cleanerDTO);
        List<City> cities = cityService.getAllByIds(cleanerDTO.getCitiesId());
        cleaner.setCities(cities);
        cleanerService.addCleaner(cleaner);
        return ResponseEntity.ok("Cleaner added.");
    }

    @PostMapping("/edit")
    public ResponseEntity<String> postEditCleaner(@RequestBody CleanerDTO cleanerDTO) {
        Cleaner cleaner = Cleaner.asCleanerWithIdSet(cleanerDTO);
        List<City> cities = cityService.getAllByIds(cleanerDTO.getCitiesId());
        cleaner.setCities(cities);
        cleanerService.editCleaner(cleaner);
        return ResponseEntity.ok("Cleaner edited.");
    }

    @PostMapping("/delete")
    public ResponseEntity<String> postDeleteCleaner(@RequestBody Map<String, Long> jsonRequest) {
        cleanerService.deleteCleaner(jsonRequest.get(CLEANER_ID));
        return ResponseEntity.ok("Cleaner deleted.");
    }

}
