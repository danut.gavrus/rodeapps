package com.rodeapps.app.repo;

import com.rodeapps.app.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepo extends JpaRepository<Booking, Long> { }