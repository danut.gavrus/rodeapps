package com.rodeapps.app.repo;

import com.rodeapps.app.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepo extends JpaRepository<City, Long> { }