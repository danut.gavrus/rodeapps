package com.rodeapps.app.repo;

import com.rodeapps.app.entity.Cleaner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CleanerRepo extends JpaRepository<Cleaner, Long> { }