package com.rodeapps.app.entity;

import com.rodeapps.app.dto.CleanerDTO;

import javax.persistence.*;
import java.util.List;

@javax.persistence.Entity
public class Cleaner {

    @ManyToMany
    private List<City> cities;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String email;

    private String phoneNumber;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static Cleaner asCleaner(CleanerDTO cleanerDTO) {
        Cleaner cleaner = new Cleaner();
        cleaner.setName(cleanerDTO.getName());
        cleaner.setEmail(cleanerDTO.getEmail());
        cleaner.setPhoneNumber(cleanerDTO.getPhoneNumber());
        return cleaner;
    }

    public static Cleaner asCleanerWithIdSet(CleanerDTO cleanerDTO) {
        Cleaner cleaner = new Cleaner();
        cleaner.setId(cleanerDTO.getId());
        cleaner.setName(cleanerDTO.getName());
        cleaner.setEmail(cleanerDTO.getEmail());
        cleaner.setPhoneNumber(cleanerDTO.getPhoneNumber());
        return cleaner;
    }

}
