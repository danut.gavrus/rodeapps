package com.rodeapps.app.entity;

import com.rodeapps.app.dto.BookingDTO;

import javax.persistence.*;

@javax.persistence.Entity
public class Booking {

    @ManyToOne
    private Cleaner cleaner;

    @ManyToOne
    private City city;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long date;

    private String customerName;

    private String customerEmail;

    private String customerPhoneNumber;

    public Cleaner getCleaner() {
        return cleaner;
    }

    public void setCleaner(Cleaner cleaner) {
        this.cleaner = cleaner;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public static Booking asBooking(BookingDTO bookingDTO) {
        Booking booking = new Booking();
        booking.setDate(bookingDTO.getDate());
        booking.setCustomerName(bookingDTO.getCustomerName());
        booking.setCustomerEmail(bookingDTO.getCustomerEmail());
        booking.setCustomerPhoneNumber(bookingDTO.getCustomerPhoneNumber());
        return booking;
    }

}
