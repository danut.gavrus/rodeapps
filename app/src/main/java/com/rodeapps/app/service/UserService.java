package com.rodeapps.app.service;

import com.rodeapps.app.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // We look in the database to see if manager account exists, otherwise we create it
        String MANAGER = "manager";
        com.rodeapps.app.entity.User user = userRepo.findByUsername(MANAGER);
        if (user == null) {
            user = new com.rodeapps.app.entity.User();
            user.setUsername(MANAGER);
            user.setPassword(MANAGER);
            userRepo.save(user);
        }

        return new User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

}
