package com.rodeapps.app.service;

import com.rodeapps.app.entity.Booking;
import com.rodeapps.app.repo.BookingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookingService {

    @Autowired
    private BookingRepo bookingRepo;

    public void addBooking(Booking booking) {
        bookingRepo.save(booking);
    }

    public List<Booking> getAll() {
        return bookingRepo.findAll();
    }

}
