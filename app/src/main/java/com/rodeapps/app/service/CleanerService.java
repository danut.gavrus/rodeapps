package com.rodeapps.app.service;

import com.rodeapps.app.entity.City;
import com.rodeapps.app.entity.Cleaner;
import com.rodeapps.app.repo.CleanerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CleanerService {

    @Autowired
    private CleanerRepo cleanerRepo;

    public void addCleaner(Cleaner cleaner) {
        cleanerRepo.save(cleaner);
    }

    public void editCleaner(Cleaner cleaner) {
        cleanerRepo.save(cleaner);
    }

    public void deleteCleaner(Long id) {
        cleanerRepo.deleteById(id);
    }

    public List<Cleaner> getCleanersByCity(City city) {
        List<Cleaner> cleaners = cleanerRepo.findAll();
        cleaners.removeIf(cleaner -> !cleaner.getCities().contains(city));
        return cleaners;
    }

    public Cleaner getById(Long id) {
        return cleanerRepo.getById(id);
    }

}
