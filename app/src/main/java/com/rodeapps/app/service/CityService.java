package com.rodeapps.app.service;

import com.rodeapps.app.entity.City;
import com.rodeapps.app.repo.CityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CityService {

    @Autowired
    private CityRepo cityRepo;

    public void addCity(City city) {
        cityRepo.save(city);
    }

    public void editCity(City city) {
        cityRepo.save(city);
    }

    public void deleteCity(Long id) {
        cityRepo.deleteById(id);
    }

    public List<City> getAllByIds(List<Long> ids) {
        return cityRepo.findAllById(ids);
    }

    public City getById(Long id) {
        return cityRepo.getById(id);
    }

}
