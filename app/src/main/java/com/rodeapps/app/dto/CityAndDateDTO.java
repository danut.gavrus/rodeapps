package com.rodeapps.app.dto;

public class CityAndDateDTO {

    private Long cityId;

    private Long date;

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
