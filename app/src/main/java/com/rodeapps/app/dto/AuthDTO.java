package com.rodeapps.app.dto;

public class AuthDTO {

    private String jwt;

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

}
