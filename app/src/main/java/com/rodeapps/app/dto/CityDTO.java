package com.rodeapps.app.dto;

import com.rodeapps.app.entity.City;

public class CityDTO {

    private Long id;

    private String name;

    private String zipCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public static CityDTO asCityDTO(City city) {
        CityDTO cityDTO = new CityDTO();
        cityDTO.setName(city.getName());
        cityDTO.setZipCode(city.getZipCode());
        return cityDTO;
    }

}
