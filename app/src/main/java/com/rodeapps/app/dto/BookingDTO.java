package com.rodeapps.app.dto;

import com.rodeapps.app.entity.Booking;

public class BookingDTO {

    private Long cleanerId;

    private Long cityId;

    private CleanerDTO cleanerDTO;

    private CityDTO cityDTO;

    private Long date;

    private String customerName;

    private String customerEmail;

    private String customerPhoneNumber;

    public Long getCleanerId() {
        return cleanerId;
    }

    public void setCleanerId(Long cleanerId) {
        this.cleanerId = cleanerId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public CleanerDTO getCleanerDTO() {
        return cleanerDTO;
    }

    public void setCleanerDTO(CleanerDTO cleanerDTO) {
        this.cleanerDTO = cleanerDTO;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public static BookingDTO asBookingDTO(Booking booking) {
        BookingDTO bookingDTO = new BookingDTO();
        bookingDTO.setCleanerDTO(CleanerDTO.asCleanerDTO(booking.getCleaner()));
        bookingDTO.setCityDTO(CityDTO.asCityDTO(booking.getCity()));
        bookingDTO.setDate(booking.getDate());
        bookingDTO.setCustomerName(booking.getCustomerName());
        bookingDTO.setCustomerEmail(booking.getCustomerEmail());
        bookingDTO.setCustomerPhoneNumber(booking.getCustomerPhoneNumber());
        return bookingDTO;
    }

}
