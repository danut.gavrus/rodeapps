package com.rodeapps.app.dto;

import com.rodeapps.app.entity.Cleaner;

import java.util.List;

public class CleanerDTO {

    private List<Long> citiesId;

    private Long id;

    private String name;

    private String email;

    private String phoneNumber;

    public List<Long> getCitiesId() {
        return citiesId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static CleanerDTO asCleanerDTO(Cleaner cleaner) {
        CleanerDTO cleanerDTO = new CleanerDTO();
        cleanerDTO.setName(cleaner.getName());
        cleanerDTO.setEmail(cleaner.getEmail());
        cleanerDTO.setPhoneNumber(cleaner.getPhoneNumber());
        return cleanerDTO;
    }

}
